# Svelte

## Usage
```bash
cd svelte
yarn
yarn dev
```

## Subscribe configuration
```javascript
{
  "http": { "port": 8888 },
  "websocket": { "port": 8888 }
}
```