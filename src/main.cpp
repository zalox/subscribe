#include <config.hpp>
#include <subscribe.hpp>

int main() {
  config config;
  return Application::get(config).run();
}
