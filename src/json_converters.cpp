#include <json_converters.hpp>

void convert::from_json(const json &json, HttpServer::Config &config) {
  config.port = json.value("port", config.port);
  config.timeout_request =
      json.value("timeout_request", config.timeout_request);
  config.timeout_content =
      json.value("timeout_content", config.timeout_content);
  config.max_request_streambuf_size = json.value(
      "max_request_streambuf_size", config.max_request_streambuf_size);
  config.address = json.value("address", config.address);
  config.reuse_address = json.value("reuse_address", config.reuse_address);
  config.fast_open = json.value("fast_open", config.fast_open);
};

void convert::from_json(const json &json, WSServer::Config &config) {
  config.port = json.value("port", config.port);
  config.timeout_request =
      json.value("timeout_request", config.timeout_request);
  config.timeout_idle = json.value("timeout_idle", config.timeout_idle);
  config.address = json.value("address", config.address);
  config.reuse_address = json.value("reuse_address", config.reuse_address);
  config.fast_open = json.value("fast_open", config.fast_open);
  config.max_message_size =
      json.value("max_mesage_size", config.max_message_size);
  config.thread_pool_size =
      json.value("thread_pool_size", config.thread_pool_size);

  if (json.contains("header")) {
    std::vector<std::vector<std::string>> headers = json.at("header");
    for (auto &header : headers) {
      config.header.emplace(std::make_pair(header.at(0), header.at(1)));
    }
  }
};

void convert::from_json(const json &json, SecureWSServer::Config &config) {
  config.port = json.value("port", config.port);
  config.timeout_request =
      json.value("timeout_request", config.timeout_request);
  config.timeout_idle = json.value("timeout_idle", config.timeout_idle);
  config.address = json.value("address", config.address);
  config.reuse_address = json.value("reuse_address", config.reuse_address);
  config.fast_open = json.value("fast_open", config.fast_open);
  config.max_message_size =
      json.value("max_mesage_size", config.max_message_size);
  config.thread_pool_size =
      json.value("thread_pool_size", config.thread_pool_size);

  if (json.contains("header")) {
    std::vector<std::vector<std::string>> headers = json.at("header");
    for (auto &header : headers) {
      config.header.emplace(std::make_pair(header.at(0), header.at(1)));
    }
  }
};

void convert::from_json(const json &json, HttpsServer::Config &config) {
  config.port = json.value("port", config.port);
  config.timeout_request =
      json.value("timeout_request", config.timeout_request);
  config.timeout_content =
      json.value("timeout_content", config.timeout_content);
  config.max_request_streambuf_size = json.value(
      "max_request_streambuf_size", config.max_request_streambuf_size);
  config.address = json.value("address", config.address);
  config.reuse_address = json.value("reuse_address", config.reuse_address);
  config.fast_open = json.value("fast_open", config.fast_open);
};