#include <http.hpp>
#include <json_converters.hpp>
#include <subscribe.hpp>

void Application::web_server_started(std::size_t port) {
  std::cout << "Web server started and listening on " << port << ".\n";
};

void Application::secure_web_server_started(std::size_t port) {
  std::cout << "Secure web server started and listening on " << port << ".\n";
};

Application::Application(const nlohmann::json &cfg) {
  convert::from_json(cfg.value("http", json::object()), http_server.config);
  if (cfg.contains("https")) {
    const auto https = cfg["https"];
    if (https.contains("cert") && https.contains("key")) {
      https_server = std::make_shared<HttpsServer>(https["cert"], https["key"]);
      sws_server =
          std::make_shared<SecureWSServer>(https["cert"], https["key"]);
    }
  }

  if (https_server) {
    convert::from_json(cfg.value("https", json::object()),
                       https_server->config);
    convert::from_json(cfg.value("websocket", json::object()),
                       sws_server->config);
    sws_server->endpoint["^.*$"];
  } else {
    ws_server = std::make_shared<WSServer>();
    convert::from_json(cfg.value("websocket", json::object()),
                       ws_server->config);
    ws_server->endpoint["^.*$"];
  }
}

int Application::run() {
  auto store = json::object();
  std::vector<std::thread> servers;

  if (https_server) {
    https_server->default_resource["GET"] =
        [&](std::shared_ptr<HttpsServer::Response> response,
            std::shared_ptr<HttpsServer::Request>) {
          response->write(Status::success_ok, store.dump(),
                          {header_access_control, header_application_data});
        };

    https_server->default_resource["POST"] =
        [&](std::shared_ptr<HttpsServer::Response> response,
            std::shared_ptr<HttpsServer::Request> request) {
          json data;
          try {
            request->content >> data;
          } catch (...) {
            return response::bad_json(response);
          }
          if (data.is_object()) {
            store.update(data);
            if (sws_server) {
              for (auto &a_connection : sws_server->get_connections()) {
                a_connection->send(data.dump());
              }
            }
            return response->write(Status::success_no_content);
          }
          return response->write(Status::client_error_bad_request);
        };

    https_server->on_upgrade =
        [=](std::unique_ptr<SimpleWeb::HTTPS> &socket,
            std::shared_ptr<HttpsServer::Request> request) {
          auto connection =
              std::make_shared<SecureWSServer::Connection>(std::move(socket));
          connection->method = std::move(request->method);
          connection->path = std::move(request->path);
          connection->http_version = std::move(request->http_version);
          connection->header = std::move(request->header);
          sws_server->upgrade(connection);
        };

    servers.emplace_back(
        [&]() { https_server->start(secure_web_server_started); });
  }

  http_server.default_resource["GET"] =
      [&](std::shared_ptr<HttpServer::Response> response, ...) {
        if (https_server) {
          return response::https_required(response);
        }
        response->write(Status::success_ok, store.dump(),
                        {header_access_control, header_application_data});
      };

  http_server.default_resource["POST"] =
      [&](std::shared_ptr<HttpServer::Response> response,
          std::shared_ptr<HttpServer::Request> request) {
        if (https_server) {
          return response::https_required(response);
        }

        json data;
        try {
          request->content >> data;
        } catch (...) {
          return response::bad_json(response);
        }
        if (data.is_object()) {
          store.update(data);
          const auto str = data.dump();
          if (ws_server) {
            for (auto &a_connection : ws_server->get_connections()) {
              a_connection->send(str);
            }
          }
          return response->write(Status::success_no_content);
        }
        return response->write(Status::client_error_bad_request);
      };

  http_server.on_upgrade = [=](std::unique_ptr<SimpleWeb::HTTP> &socket,
                               std::shared_ptr<HttpServer::Request> request) {
    auto connection = std::make_shared<WSServer::Connection>(std::move(socket));
    connection->method = std::move(request->method);
    connection->path = std::move(request->path);
    connection->http_version = std::move(request->http_version);
    connection->header = std::move(request->header);
    ws_server->upgrade(connection);
  };

  servers.emplace_back([&]() { http_server.start(web_server_started); });

  for (auto &server : servers) {
    server.join();
  }

  return 0;
}

Application &Application::get(const json &config) {
  static Application app(config);
  return app;
}
