#include <http.hpp>
#include <json.hpp>

const auto https_required_msg = json::object({
    {"message", "100: Your server is configured to use https, but a "
                "request on http was received."},
});

const auto bad_json_msg = json::object({
    {"message", "101: Your request was not a valid JSON document."},
});

const auto bad_object_msg = json::object({
    {"message", "102: Your request was not a valid store."},
});

void response::https_required(std::shared_ptr<HttpServer::Response> response) {
  return response->write(Status::client_error_bad_request,
                         https_required_msg.dump(),
                         {header_access_control, header_application_data});
}

void response::https_required(std::shared_ptr<HttpsServer::Response> response) {
  return response->write(Status::client_error_bad_request,
                         https_required_msg.dump(),
                         {header_access_control, header_application_data});
}

void response::bad_json(std::shared_ptr<HttpServer::Response> response) {
  return response->write(Status::client_error_bad_request, bad_json_msg.dump(),
                         {header_access_control, header_application_data});
}

void response::bad_json(std::shared_ptr<HttpsServer::Response> response) {
  return response->write(Status::client_error_bad_request, bad_json_msg.dump(),
                         {header_access_control, header_application_data});
}


void response::bad_object(std::shared_ptr<HttpServer::Response> response) {
  return response->write(Status::client_error_bad_request,
                         bad_object_msg.dump(),
                         {header_access_control, header_application_data});
}

void response::bad_object(std::shared_ptr<HttpsServer::Response> response) {
  return response->write(Status::client_error_bad_request,
                         bad_object_msg.dump(),
                         {header_access_control, header_application_data});
}
