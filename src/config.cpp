#include <config.hpp>
#include <fstream>
#include <iostream>
#include <unistd.h>

void config::create_config_directory() {
  std::error_code ec;
  fs::create_directories(get_config_path(), ec);
  if (ec) {
    std::cerr << "Unable to create configuration directory:\n" << ec.message();
  }
}

fs::path &config::get_config_path() {
  static fs::path p("");
  if (p.native().length() != 0) {
    return p;
  }
  const int uid = getuid();
  if (uid == 0) {
    p = fs::path("/") / "etc" / "subscribe";
  } else if (const auto ptr = std::getenv("HOME")) {
    p = fs::path(ptr) / ".config" / "subscribe";
  }
  return p;
}

void config::create_config_file(const fs::path &config_file_path) {
  try {
    std::ofstream f(config_file_path);
    f << json::object();
  } catch (const std::exception &e) {
    std::cerr << "Could not create config file:\n" << e.what() << std::endl;
  }
}

void config::load_config_file(const fs::path &config_file_path) {
  std::ifstream i(config_file_path.c_str());
  i >> *this;
}

config::config() : json(json::object()) {
  if (!fs::exists(get_config_path())) {
    create_config_directory();
  }
  const auto config_file_path = get_config_path() / "config.json";
  if (!fs::exists(config_file_path)) {
    create_config_file(config_file_path);
  } else {
    load_config_file(config_file_path);
  }
}