#pragma once

#include <server_http.hpp>
#include <server_https.hpp>
#include <server_ws.hpp>
#include <server_wss.hpp>

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
using WSServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using Status = SimpleWeb::StatusCode;
using HttpsServer = SimpleWeb::Server<SimpleWeb::HTTPS>;
using SecureWSServer = SimpleWeb::SocketServer<SimpleWeb::WSS>;
