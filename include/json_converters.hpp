#pragma once
#include <json.hpp>
#include <web_server.hpp>

class convert {
public:
  static void from_json(const json &, HttpServer::Config &);
  static void from_json(const json &, HttpsServer::Config &);
  static void from_json(const json &, WSServer::Config &);
  static void from_json(const json &, SecureWSServer::Config &);
};
