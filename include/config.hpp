#pragma once
#include <experimental/filesystem>
#include <json.hpp>

namespace fs = std::experimental::filesystem;

class config : public json {
  static fs::path &get_config_path();
  void create_config_directory();
  void create_config_file(const fs::path &config_file_path);
  void load_config_file(const fs::path &config_file_path);

public:
  config();
};