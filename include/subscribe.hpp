#pragma once
#include <json.hpp>
#include <web_server.hpp>

class Application {
  HttpServer http_server;

  static void web_server_started(std::size_t port);
  static void secure_web_server_started(std::size_t port);
  Application() = delete;
  Application(const json &cfg);

public:
  std::shared_ptr<HttpsServer> https_server = nullptr;
  std::shared_ptr<WSServer> ws_server = nullptr;
  std::shared_ptr<SecureWSServer> sws_server = nullptr;

  static Application &get(const json &config);
  int run();
};
