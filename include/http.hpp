#pragma once
#include <web_server.hpp>

const auto header_application_data =
    std::make_pair("Content-Type", "application/json");
const auto header_access_control =
    std::make_pair("Access-Control-Allow-Origin", "*");

class response {
public:
  static void https_required(std::shared_ptr<HttpServer::Response> response);
  static void https_required(std::shared_ptr<HttpsServer::Response> response);
  static void bad_json(std::shared_ptr<HttpServer::Response> response);
  static void bad_json(std::shared_ptr<HttpsServer::Response> response);
  static void bad_object(std::shared_ptr<HttpServer::Response> response);
  static void bad_object(std::shared_ptr<HttpsServer::Response> response);
};