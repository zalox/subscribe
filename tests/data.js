const https = require('https');

// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const doRequest = async () => {
  const random =
      (low, high) => { return Math.floor(Math.random() * (high - low) + low); };

  let items = [];

  for (let i = 0; i < random(1, 30); i++) {
    let k = new Date(random(1979, 2020));
    k.setYear(random(2000, 2020));
    items.push(k);
  }

  const r = new Promise((resolve) => {
    const postData = JSON.stringify({items});
    const req =
        https.request('https://localhost', {
          port : 8443,
          method : 'POST',
          path : '/',
          headers : {
            'Content-Type' : 'application/json',
            'Content-Length' : Buffer.byteLength(postData)
          },
          rejectUnauthorized : false,
        },
                      (response) => { response.on('end', () => resolve()); });
    req.write(postData);
    req.end();
  });
  const l = await r;
};

while (true) {
  doRequest();
}
