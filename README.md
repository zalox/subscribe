# Subscribe

## About
Subscribe is a simple in memory JSON store.

## Dependencies
* [simple-web-server](http://gitlab.com/eidheim/Simple-Web-Server/)
* [simple-websocket-server](http://gitlab.com/eidheim/Simple-WebSocket-Server/)
* [json](https://github.com/nlohmann/json)

## Build
```sh
git clone https://gitlab.com/zalox/subscribe --recursive
cd subscribe
mkdir build
cd build
cmake ..
make -j$(nproc)
```

## Usage
See [clients](https://gitlab.com/zalox/subscribe/-/blob/master/share/doc/clients)

## Configuration
NOTE: The default value will be used if a field is omitted.
Headers are on the format `["Accept", "*"]`;
The configuration is passed directly to [the webserver](https://gitlab.com/eidheim/Simple-Web-Server)
and [websocket.](https://gitlab.com/eidheim/Simple-WebSocket-Server)

```javascript
{
  "http": {
    "port": 80,
    // If io_service is not set, number of threads that the server will use when start() is called.
    // Defaults to 1 thread.
    "thread_pool_size": 1,
    // Timeout on request completion. Defaults to 5 seconds.
    "timeout_request": 5,
    // Timeout on request/response content completion. Defaults to 300 seconds.
    "timeout_content": 300,
    // Maximum size of request stream buffer. Defaults to architecture maximum.
    // Reaching this limit will result in a message_size error code.
    // default: (std::numeric_limits<std::size_t>::max)()
    "max_request_streambuf_size": 18446744073709551615,
    // IPv4 address in dotted decimal form or IPv6 address in hexadecimal notation.
    // If empty, the address will be any address.
    "address": "",
    // Set to false to avoid binding the socket to an address that is already in use. Defaults to true.
    "reuse_address": true,
    // Make use of RFC 7413 or TCP Fast Open (TFO)
    "fast_open": false
  },
  {
  "https": {
    "port": 443,
    // If io_service is not set, number of threads that the server will use when start() is called.
    // Defaults to 1 thread.
    "thread_pool_size": 1,
    // Timeout on request completion. Defaults to 5 seconds.
    "timeout_request": 5,
    // Timeout on request/response content completion. Defaults to 300 seconds.
    "timeout_content": 300,
    // Maximum size of request stream buffer. Defaults to architecture maximum.
    // Reaching this limit will result in a message_size error code.
    // default: (std::numeric_limits<std::size_t>::max)()
    "max_request_streambuf_size": 18446744073709551615,
    // IPv4 address in dotted decimal form or IPv6 address in hexadecimal notation.
    // If empty, the address will be any address.
    "address": "",
    // Set to false to avoid binding the socket to an address that is already in use. Defaults to true.
    "reuse_address": true,
    // Make use of RFC 7413 or TCP Fast Open (TFO)
    "fast_open": false
  },
  "websocket": {
     // Port number to use. Defaults to 80 for HTTP and 443 for HTTPS.
    "port": 80,
     // If io_service is not set, number of threads that the server will use when start() is called.
     // Defaults to 1 thread.
     "thread_pool_size": 1,
     // Timeout on request completion. Defaults to 5 seconds.
     "timeout_request": 5,
     // Timeout on request/response content completion. Defaults to 300 seconds.
     "timeout_idle": 0,
     // Maximum size of request stream buffer. Defaults to architecture maximum.
     // Reaching this limit will result in a message_size error code.
     // default: (std::numeric_limits<std::size_t>::max)()
     "max_message_size": 18446744073709551615,
     // IPv4 address in dotted decimal form or IPv6 address in hexadecimal notation.
     // If empty, the address will be any address.
     "address": "",
     // Set to false to avoid binding the socket to an address that is already in use. Defaults to true.
     "reuse_address": true,
     // Make use of RFC 7413 or TCP Fast Open (TFO)
     "fast_open": false,
     // Additional header fields to send when performing WebSocket handshake
     "headers": []
  }
}

```
### Example configuration
A very simple https configuration (recommended):
``` javascript
{
  "https": {
    "cert": "/etc/ssl/cert.crt",
    "key": "/etc/ssl/key.pem"
  }
}

```
A very simple http configuration (default):
``` javascript
{}
```

# Troubleshooting

## Errors
### Client errors
```yaml
;; The server will return this error message if the server is configured with
;; https and the client attempts to access it over http.
100: Your server is configured to use https, but a request on http was recived.

;; The server will return this error message if the body of your request is
;; invalid json. Consider using a validator to find your mistake.
101: Your request was not a valid JSON document.

;; The server will return this error message if the root of your JSON request
;; is not {}.
102: Your document was not a valid JSON object.
```